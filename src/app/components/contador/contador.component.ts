import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {

  numero: number = 0;
  mensaje: boolean = false; 

  constructor() { }

  ngOnInit(): void {
  }

  sumar() {
    if (this.numero === 50) {
      this.mensaje = true;
    }else {
      this.numero += 1;
      this.mensaje = false
    }   
  };

  restar() {
    if (this.numero ===0) {
      this.mensaje = true;
    }else {
      this.numero -= 1;
      this.mensaje = false;
    }   
  };

}
